import streamlit as st
import pandas as pd
import numpy as np

st.set_page_config(layout="wide")
def local_css(file_name):
    with open(file_name) as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)
local_css("style.css")

st.title("No Refinancing: Pay off Loans or Contribute to Stock Market?")
#st.write(f"Total Initial NET WORTH = ${37000-304000:,}")


# DURING DEFERMENT PERIOD - No required loan payments.
st.header("DURING DEFERMENT PERIOD")
st.write("During deferment periods, 1. you are not required to make payments 2. interest still accrues, but is not rolled into principle while in deferment.")

fill0, fill1, global_opts, fill2, fill3 = st.columns((1,1,10,1,1))
with fill0:
    st.write("")
with fill1:
    st.write("")
with fill2:
    st.write("")
with fill3:
    st.write("")
with global_opts:
# While in Deferment Calculations
    m_savings = st.slider(label = 'Select monthly savings to be distributed', min_value = 0.0, max_value = 3500.0, step = 100.0, value=1500.0)
    loan_contrib = st.slider(label = 'Percentage of savings to loan payments', min_value = 0.0, max_value = 1.0, step = 0.1, value=0.0)
    time_pd = st.slider(label = 'How many years?', min_value = 0, max_value = 5, step = 1, value = 3)
    num_months = 12*time_pd

defer_left_column, defer_right_column = st.columns(2)
# Left column = Loan Amortization payoff schedule
with defer_left_column:
    st.write("Loan Amortization Payoff Schedule")
    # Total loan amount
    loan_payoff_amt = st.slider(label='Total loan amount', min_value=0, max_value=350000, step=1000, value=20000)
    interest_accrued = st.slider(label='Interest accrued amount', min_value=0, max_value=100000, step=1000, value=5000)
    loan_interest = st.slider(label='Interest rate on loan', min_value=0.03, max_value=0.10, step=0.001, value=0.069)
    amort_df = pd.DataFrame({"Balance": loan_payoff_amt, "Account Interest Accrued": interest_accrued,
                             "Added Interest Accrued": loan_payoff_amt * loan_interest / 12,
                             "Total Payment": m_savings * loan_contrib,
                             "Ending Balance": loan_payoff_amt + interest_accrued - (m_savings * loan_contrib - loan_payoff_amt * loan_interest / 12)},
                            index=[0])
    interest_acc = amort_df.loc[0, "Account Interest Accrued"]
    for m in range(1, num_months):
        beg_bal = loan_payoff_amt
        interest_m = beg_bal * loan_interest / 12
        total_payment = m_savings * loan_contrib
        interest_acc = interest_acc + interest_m - total_payment
        end_bal = beg_bal + interest_acc
        new_month = {"Balance": beg_bal, "Account Interest Accrued": interest_acc, "Added Interest Accrued": interest_m,
                     "Total Payment": total_payment, "Ending Balance": end_bal}
        amort_df = amort_df.append(new_month, ignore_index=True)
    if st.checkbox('Show loan payoff amortization schedule over ' + str(num_months) + ' months.', key="amort_m1"):
        st.dataframe(amort_df.style.format("${:,.2f}"))
    l_delta = round(list(amort_df["Balance"])[0] + list(amort_df["Account Interest Accrued"])[0] - list(amort_df["Ending Balance"])[-1], 2)
    st.write(f"Total paid off = ${l_delta:,}")
    st.write(f"Total still owe = ${round(list(amort_df['Ending Balance'])[-1], 2):,}")

# Right column = Schwab earnings
with defer_right_column:
    st.write("Schwab Earnings Schedule")
    # Schwab start value
    schwab_init = st.slider(label='Start value Schwab account', min_value=0, max_value=100000, step=1000, value=30000)
    r_return = st.slider(label='Select annual rate of return expected', min_value=0.0, max_value=0.30, step=0.01,
                         value=0.11)
    m_r_return = r_return / 12
    schwab_df = pd.DataFrame({"Balance": schwab_init, "Interest Earned": schwab_init * m_r_return,
                              "Total Contribution": m_savings * (1 - loan_contrib),
                              "Ending Balance": schwab_init + (m_savings * (1 - loan_contrib)) + (
                                          schwab_init * m_r_return)},
                             index=[0])
    end_bal = schwab_df.loc[0, "Ending Balance"]
    for m in range(1, num_months):
        beg_bal = end_bal
        interest_earned = beg_bal * m_r_return
        total_contribution = m_savings * (1 - loan_contrib)
        end_bal = beg_bal + total_contribution + interest_earned
        new_month = {"Balance": beg_bal, "Interest Earned": interest_earned,
                     "Total Contribution": total_contribution, "Ending Balance": end_bal}
        schwab_df = schwab_df.append(new_month, ignore_index=True)
    if st.checkbox('Show Schwab earnings over ' + str(num_months) + ' months.', key="schwab_m1"):
        st.dataframe(schwab_df.style.format("${:,.2f}"))
    s_delta = round(list(schwab_df["Ending Balance"])[-1] - list(schwab_df["Balance"])[0], 2)
    st.write(f"Total earned = ${s_delta:,}")
net_worth = round(list(schwab_df["Ending Balance"])[-1] - list(amort_df["Ending Balance"])[-1], 2)
st.write(f"Total NET WORTH = ${net_worth:,}")







# AFTER DEFERMENT PERIOD - Loan payments are required
st.header("AFTER DEFERMENT PERIOD")
st.write("After deferment periods end, 1. interest accrued almost always gets rolled into principle and 2. you are now required to pay minimum monthly payments.")
fill01, fill11, global_opts2, fill21, fill31 = st.columns((1,1,10,1,1))
with fill01:
    st.write("")
with fill11:
    st.write("")
with fill21:
    st.write("")
with fill31:
    st.write("")
with global_opts2:
    # Determine discretionary income
    st.write("Determine discretionary income and minimum monthly payment.")
    d_income = st.slider(label = 'Total annual household income', min_value = 0, max_value = 500000, step = 10000, value = 100000)
    poverty_level_state = st.slider(label = 'Poverty level for state for household size?', min_value = 20000, max_value = 40000, step = 1000, value=21000)
    min_m_loan_payment = 0.15/12*(d_income - (poverty_level_state*1.5))
    st.write("Based on household income, in a non-deferment period, your discretionary income requires you to pay $", str(round(min_m_loan_payment, 2)), " per month.")

    # Total loan amount
    loan_payoff_amt2 = list(amort_df["Ending Balance"])[-1]
    # loan_interest
    loan_interest2 = st.slider(label='Interest rate on loan', min_value=0.020, max_value=0.100, step=0.001, value=0.044)
    # loan term
    time_pd2 = st.slider(label = 'How many years?', min_value = 0, max_value = 12, step = 1, value = 12)
    num_months2 = 12*time_pd2

    r = loan_interest2 / 12
    amort_payment = loan_payoff_amt2 * (r*(1+r)**num_months2) / ((1+r)**num_months2 - 1)
    #st.write(amort_payment)
    if min_m_loan_payment>=amort_payment:
        min_m_loan_payment = amort_payment
    st.write("Given the remaining loan balance ($", str(round(loan_payoff_amt2, 2)), "), repayment period (", str(num_months2), " months) and your discretionary income, your minimum monthly payment is $", str(round(min_m_loan_payment, 2)))

    m_savings2 = st.slider(label = 'Select monthly savings to be distributed', min_value = min_m_loan_payment, max_value = 7500.0, step = 100.0, value=3500.0)
    loan_contrib2 = st.slider(label = 'Percentage of savings made to loan payments', min_value = (min_m_loan_payment / m_savings2), max_value = 1.0, step = 0.02, value=min_m_loan_payment/m_savings2)

left_column, right_column = st.columns(2)
# Left column = Loan Amortization payoff schedule
with left_column:
    st.write("Loan Amortization Payoff Schedule")
    amort_df2 = pd.DataFrame({"Balance": loan_payoff_amt2, "Interest Paid": loan_payoff_amt2 * loan_interest2 / 12,
                             "Total Payment": m_savings2 * loan_contrib2,
                             "Ending Balance": loan_payoff_amt2 - (m_savings2 * loan_contrib2 - loan_payoff_amt2 * loan_interest2 / 12)},
                            index=[0])
    end_bal = amort_df2.loc[0, "Ending Balance"]
    for m in range(1, num_months2):
        beg_bal = end_bal
        interest_paid = beg_bal * loan_interest2 / 12
        total_payment = m_savings2 * loan_contrib2
        end_bal = beg_bal - (total_payment - interest_paid)
        new_month = {"Balance": beg_bal, "Interest Paid": interest_paid,
                     "Total Payment": total_payment, "Ending Balance": end_bal}
        amort_df2 = amort_df2.append(new_month, ignore_index=True)
    if st.checkbox('Show loan payoff amortization schedule over ' + str(num_months2) + ' months.', key="amort_m2"):
        st.dataframe(amort_df2.style.format("${:,.2f}"))
    l_delta2 = round(list(amort_df2["Balance"])[0] - list(amort_df2["Ending Balance"])[-1], 2)
    st.write(f"Total paid off = ${l_delta2:,}")
    st.write(f"Total still owe = ${round(list(amort_df2['Ending Balance'])[-1], 2):,}")

# Right column = Schwab earnings
with right_column:
    st.write("Schwab Earnings Schedule")
    # Schwab start value
    schwab_init2 = list(schwab_df["Ending Balance"])[-1]
    #r_return = # Assume same rate of return as in deferment period
    #m_r_return = r_return / 12
    schwab_df2 = pd.DataFrame({"Balance": schwab_init2, "Interest Earned": schwab_init2 * m_r_return,
                              "Total Contribution": m_savings2 * (1 - loan_contrib2),
                              "Ending Balance": schwab_init2 + (m_savings2 * (1 - loan_contrib2)) + (
                                          schwab_init2 * m_r_return)},
                             index=[0])
    end_bal = schwab_df2.loc[0, "Ending Balance"]
    for m in range(1, num_months2):
        beg_bal = end_bal
        interest_earned = beg_bal * m_r_return
        total_contribution = m_savings2 * (1 - loan_contrib2)
        end_bal = beg_bal + total_contribution + interest_earned
        new_month = {"Balance": beg_bal, "Interest Earned": interest_earned,
                     "Total Contribution": total_contribution, "Ending Balance": end_bal}
        schwab_df2 = schwab_df2.append(new_month, ignore_index=True)
    if st.checkbox('Show Schwab earnings over ' + str(num_months2) + ' months.', key="schwab_m2"):
        st.dataframe(schwab_df2.style.format("${:,.2f}"))
    s_delta2 = round(list(schwab_df2["Ending Balance"])[-1] - list(schwab_df2["Balance"])[0], 2)
    st.write(f"Total earned = ${s_delta2:,}")

net_worth2 = round(list(schwab_df2["Ending Balance"])[-1] - list(amort_df2["Ending Balance"])[-1], 2)
st.write(f"Total NET WORTH = ${net_worth2:,}")





st.title("With Refinancing: Pay off Loans + Extra Savings Contribute to Stock Market?")
#st.header("Assume a 3.31% refinance interest with ELFI on full loan amount 304k over 15 years.")
fill02, fill12, global_opts3, fill22, fill32 = st.columns((1,1,10,1,1))
with fill02:
    st.write("")
with fill12:
    st.write("")
with fill22:
    st.write("")
with fill32:
    st.write("")
with global_opts3:
    loan_interest3 = st.slider(label="Refinance interest rate", min_value = 0.03, max_value = 0.045, step=0.01, value=0.0331)
    time_pd3 = st.slider(label = 'How many years?', min_value = 0, max_value = 15, step = 1, value = 15)
    num_months3 = 12*time_pd3
    P = loan_payoff_amt+interest_accrued
    r = loan_interest3 / 12
    required_refi_payment = P * (r*(1+r)**num_months3) / ((1+r)**num_months3 - 1)
    st.write("Minimum required monthly payments to pay off loan ($", str(round(loan_payoff_amt+interest_accrued, 2)), ") is: $" + str(round(required_refi_payment, 2)))
    m_savings3_1 = st.slider(label = 'Select monthly savings first 3 years', min_value = 1500.0, max_value = 3500.0, step = 100.0, value=1500.0)
    loan_contrib3_1 = st.slider(label = 'Percentage of savings applied to loan payments first 3 years', min_value = (required_refi_payment/m_savings3_1), max_value = 1.0, step = 0.02, value=(required_refi_payment/m_savings3_1))
    m_savings3_2 = st.slider(label = 'Select monthly savings remaining years', min_value = 1500.0, max_value = 7000.0, step = 100.0, value=3500.0)
    loan_contrib3_2 = st.slider(label = 'Percentage of savings applied to loan payments remaining years', min_value = (required_refi_payment/m_savings3_2), max_value = 1.0, step = 0.02, value=(required_refi_payment/m_savings3_2))

refi_left_column, refi_right_column = st.columns(2)
# Left column = Loan Amortization payoff schedule
with refi_left_column:
    st.write("Refinanced Loan Amortization Payoff Schedule")
    # Total loan amount
    # loan_payoff_amt # Assume same as in non-refinanced deferment period
    amort_df3 = pd.DataFrame({"Balance": loan_payoff_amt+interest_accrued, "Interest Paid": (loan_payoff_amt+interest_accrued) * loan_interest3 / 12,
                             "Total Payment": m_savings3_1 * loan_contrib3_1,
                             "Ending Balance": loan_payoff_amt+interest_accrued - (m_savings3_1 * loan_contrib3_1 - loan_payoff_amt * loan_interest3 / 12)},
                            index=[0])
    end_bal = amort_df3.loc[0, "Ending Balance"]
    for m in range(1, num_months3):
        beg_bal = end_bal
        interest_paid = beg_bal * loan_interest3 / 12
        total_payment = required_refi_payment
        end_bal = beg_bal - (total_payment - interest_paid)
        new_month = {"Balance": beg_bal, "Interest Paid": interest_paid,
                     "Total Payment": total_payment, "Ending Balance": end_bal}
        amort_df3 = amort_df3.append(new_month, ignore_index=True)
    if st.checkbox('Show loan payoff amortization schedule over ' + str(num_months3) + ' months.', key="amort_m3"):
        st.dataframe(amort_df3.style.format("${:,.2f}"))
    l_delta3 = round(list(amort_df3["Balance"])[0] - list(amort_df3["Ending Balance"])[-1], 2)
    st.write(f"Total paid off = ${l_delta3:,}")
    st.write(f"Total still owe = ${round(list(amort_df3['Ending Balance'])[-1], 2):,}")

# Right column = Schwab earnings
with refi_right_column:
    st.write("Schwab Earnings Schedule")
    # Schwab start value
    # schwab_init # Assume same initial schwab value as in non-refinanced deferment period
    #r_return = # Assume same rate of return as in deferment period
    #m_r_return = r_return / 12
    schwab_df3 = pd.DataFrame({"Balance": schwab_init, "Interest Earned": schwab_init * m_r_return,
                              "Total Contribution": m_savings3_1 * (1 - loan_contrib3_1),
                              "Ending Balance": schwab_init + ((m_savings3_1-required_refi_payment) * (1 - loan_contrib3_1)) + (
                                          schwab_init * m_r_return)},
                             index=[0])
    end_bal = schwab_df3.loc[0, "Ending Balance"]
    for m in range(1, num_months3):
        beg_bal = end_bal
        interest_earned = beg_bal * m_r_return
        if m<36:
            total_contribution = (m_savings3_1) * (1 - loan_contrib3_1)
        else:
            total_contribution = (m_savings3_2) * (1 - loan_contrib3_2)
        end_bal = beg_bal + total_contribution + interest_earned
        new_month = {"Balance": beg_bal, "Interest Earned": interest_earned,
                     "Total Contribution": total_contribution, "Ending Balance": end_bal}
        schwab_df3 = schwab_df3.append(new_month, ignore_index=True)
    if st.checkbox('Show Schwab earnings over ' + str(num_months3) + ' months.', key="schwab_m3"):
        st.dataframe(schwab_df3.style.format("${:,.2f}"))
    s_delta3 = round(list(schwab_df3["Ending Balance"])[-1] - list(schwab_df3["Balance"])[0], 2)
    st.write(f"Total earned = ${s_delta3:,}")

net_worth3 = round(list(schwab_df3["Ending Balance"])[-1] - list(amort_df3["Ending Balance"])[-1], 2)
st.write(f"Total NET WORTH = ${net_worth3:,}")




# streamlit run your_script.py [-- script args]
# Ctrl+ C to quit.

# If you accidentally did Ctrl + Z, the port will still be in use by streamlit.
# Need to kill all processes listening to port 5000 (default listed in ~/.streamlit/config.toml)
# Do this with: kill -9 $(lsof -t -i:5000)
