# brainy-etc

A data science blog

## Access Gitlab Pages site
Access this static markdown blog at: https://lthistlethwaite-prnt.gitlab.io/brainy-etc/

## Access Streamlit Apps
To access streamlit apps in apps/ dir, go to the corresponding heroku site with pattern: `https://<folder-name-in-apps-directory>.herokuapp.com/`
