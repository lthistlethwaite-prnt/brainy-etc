+++
author = "Lillian Thistlethwaite"
title = "Findings from the Data Augmentation for Event Extraction (DAFEE) Project"
date = "2023-12-01"
description = "Final deliverable for the Laboratory for Analytic Sciences contract."
featured = true
tags = [
    "data augmentation", "natural language processing", "event extraction"
]
categories = [
    "analysis",
    "data science"
]
series = ["Analysis"]
thumbnail = "dafee.png"
+++

## Introduction
In 2023, I served as a Principal Investigator for a $250,000 government contract through the Laboratory for Analytic Sciences (LAS). During this contract, I executed a research plan to develop a pipeline to automatically generate synthetic data for custom event extraction. The full write up and enumeration of findings can be found on [the client's website](https://ncsu-las.org/2023/12/title-findings-from-the-data-augmentation-for-event-extraction-dafee-project/).