+++
author = "Lillian Thistlethwaite"
title = "Should I Pay Off My Student Loans or Contribute My Savings to the Stock Market?"
date = "2022-03-26"
description = "Squashing debt vs. building equity in regards to optimizing one's net worth."
featured = true
tags = [
    "app",
    "personal finance"
]
categories = [
    "analysis",
    "data science"
]
series = ["Analysis"]
thumbnail = "amortization.png"
+++

## Introduction
For the past 5 years, my husband and I have been pursuing the Public Service Loan Forgiveness program offered by the federal government for federal student loans. Recently, however, my husband and I made the decision to leave academia, and thus, the remaining years in the PSLF program will not be fulfilled. With this decision came a drastic need to change our financial strategy. Previously, our financial strategy was to pay the minimum required across all of our student loans, for 10 years, until our loans were all forgiven. Now that we have decided to leave academia, we know the remaining years that would qualify us for student loan forgiveness under the PSLF program will never be served, and thus, we are forced to pursue a different strategy to pay off or receive forgiveness on our student loans.

With student loan refinance rates at historic lows right now, we were interested in refinancing at least some of our loans immediately, but with the current political climate surrounding student loan forgiveness in Biden’s administration, and the long-term financial impact of investing less due to student loan payments, we were unsure what will ultimately be the optimal decision.

Our situation ultimately inspired the major topic of this blog post: What is the best way to maximize one’s net worth over time? Should we contribute to the stock market, or pay off student loans aggressively first, or do a mixture of both?

Importantly, the PSLF program isn't the only program offered by the federal government to pursue student loan forgiveness. There is also the Income-based Repayment (IBR) plan, which specifies that making minimum student loan payments - calculated based upon your income - over a span of 20 or 25 years will qualify you for student loan forgiveness. The big "gotcha" on the IBR forgiveness track, however, is that you will be taxed on the loan balance forgiven as if it were extra income for the year you are forgiven. During Biden's administration, they have made some changes to the IBR track specifying you will not owe in taxes if you are forgiven by 2025, but after 2025, it's looking like forgiven IBR loans will be taxed as income once again. Depending on the loan balance forgiven, the taxes owed can amount to thousands and thousands of dollars for that tax year which many people could not afford to make. For example, for a married couple filing jointly with $50,000, $100,000 or $200,000 in pre-tax income and a variable amount of student loan debt forgiven with IBR, they will owe a total federal tax bill as shown in Table 1 below.

Table 1. Household income + student loan forgiveness balance and total federal taxes owed using [NerdWallet's tax calculator](https://www.nerdwallet.com/taxes/tax-calculator).
| Household Income | IBR Income          | Income Taxes | Student Loan Taxes | Total Federal Taxes |
| ---              | ---                 | ---          | ---                | ---                 |
| $50,000          | $10,000             | $2,590       | $1,200             | $3,790              |
| $50,000          | $50,000             | $2,590       | $6,000             | $8,590              |
| $50,000          | $100,000            | $2,590       | $16,385            | $18,975             |
| $50,000          | $200,000            | $2,590       | $39,428            | $42,018             |
| $50,000          | $300,000            | $2,590       | $63,428            | $66,018             |
| $50,000          | $600,000            | $2,590       | $165,213           | $167,803            |
| $100,000         | $10,000             | $8,590       | $1,585             | $10,175             |
| $100,000         | $50,000             | $8,590       | $10,385            | $18,975             |
| $100,000         | $100,000            | $8,590       | $21,428            | $30,018             |
| $100,000         | $200,000            | $8,590       | $45,428            | $54,018             |
| $100,000         | $300,000            | $8,590       | $73,032            | $81,622             |
| $100,000         | $600,000            | $8,590       | $177,645           | $186,235            |
| $200,000         | $10,000             | $30,018      | $2,400            | $32,418             |
| $200,000         | $50,000             | $30,018      | $12,000           | $42,018             |
| $200,000         | $100,000            | $30,018      | $24,000           | $54,018             |
| $200,000         | $200,000            | $30,018      | $51,604           | $81,622             |
| $200,000         | $300,000            | $30,018      | $85,285           | $115,303            |
| $200,000         | $600,000            | $30,018      | $193,217          | $223,235            |

Whereas federal taxes due to income will be withheld from their regular paychecks, borrowers will have to save up to afford the taxes due on their IBR forgiven student loans over the course of the 20 to 25 year IBR payment period. Since the point of IBR is to relieve borrowers from punitively high monthly payments, the idea that borrowers could afford to save more than these required IBR monthly payments to be able to pay their tax bill at forgiveness, makes little sense. However, let's assume they are able to tuck away some extra savings into the stock market and they see their extra savings benefit from compound interest over a 20 or 25 year time horizon. When those borrowers go to sell stock at the time of their IBR loan forgiveness, they will be hit with [capital gains taxes](https://www.nerdwallet.com/article/taxes/capital-gains-tax-rates?fbclid=IwAR1RqG-GbkbLV1ziwDg30LfbS3Qcwa6LQJFoOe-UDlk1NZjjrrT7fAGJGaA), which, while not as punitive as income taxes, are still reasonably punitive.

Nevertheless, in this analysis, I consider the income-based repayment plan as a baseline strategy (i.e., Strategy 0) to compare against two other strategies (i.e., Strategy 1 and Strategy 2) in the effort to maximize one's net worth over a fixed time horizon. The remaining two strategies I explored are:
- Strategy 1: This first strategy uses the advantages of the student deferment period, which allows students still in school to avoid making payments while in school. Interest still accumulates on the loans, but interest is not rolled into principal until after the deferment period ends. When the student graduates, all accumulated interest rolls into principal and the best option at that point is to refinance those loans and get a lower interest rate. During the deferment period, however, you can aggressively contribute all of your savings to the stock market and see the incredible growth offered by compound interest. Given that interest rates have been 0% for almost two years at the time of this blog post (due to the COVID-19 pandemic), it was a no brainer to us to push all of our savings to the stock market during this time and we have seen incredible returns during the past two years in our portfolio as the economy has reacted, recovered and adjusted to the pandemic. However, the decision about what to do when the loan forbearance lifts was less clear.

- Strategy 2: The last strategy I explored was to refinance student loans immediately - despite qualifying for deferment while in school - to lock in a lower interest rate for the remainder of the payoff period, and to force payments to begin immediately and avoid accumulating any more interest owed. Because federal student loan interest rates are 0% right now, private refinance rates are at their all time lows, too. At the time of this blog post, we qualified for refinance rates at 3% to refinance our federal student loans. These rates will likely climb in the near future when the forbearance is lifted and even moreso after we graduate in about 3 years from now. While eliminating debt is a good way to eliminate anxiety and liability, it would take away substantial investment opportunities via the stock market to take advantage of compound interest that over long time horizons can exponentiate one’s net worth.


## Analysis Overview
Our biggest desire was to find a balance where pushing savings to the stock market could outpace any interest accumulated on our student loans. The big limitation to this approach was that selling stock typically gets taxed, so any gains in one’s net worth due to the stock market needs to take into consideration the amount of taxation selling stock to pay off student loan debt would incur for that year. In order to answer this empirical question, I included the following as variables in this analysis:

- The total principal associated with all student loans needing to be paid off (including any interest that had accumulated)
- Consolidated loan interest when the forbearance period lifts (weighted average of interest across all loans)
- The number of years of expected deferment
- Assumed yearly rate of return expected from the stock market (a conservative estimate that takes into account inflation is 0.08)
- Estimated refinance rate for loans (I recommend getting a quote from Credible.com)
- The total amount of money that could be put towards savings and/or paying off loans each month

Like all empirical questions, the best strategy to maximizing one’s net worth depends on several of the above variables. How much money one can afford to put towards savings, the total debt to be paid off, and market assumptions are all very influential in determining which strategy is smarter for one’s long term net worth. Let’s get into what the math says.

First, I’m going to assume that, at a maximum, student loan borrowers can’t afford to put more than 20-35% of their after tax income towards savings and/or loan payoff per month. For example, for household salaries of 20k-200k a year, the range of maximum monthly savings by salary range would roughly follow the values shown in Table 2. One of course should note that the values shown in Table 2 will differ slightly for households of size 2 or greater. However, the values in Table 2 can serve as general ballpark ranges for planning monthly savings goals.

Table 2. Monthly savings potential brackets based on annual household income.
| Annual Household Salary | After Tax Income | 20% Savings | 25% Savings | 30% Savings | 35% Savings |
| --- | --- | --- | --- | --- | --- |
| 20k                     | 17.5k            | $292 | $365 | $438 | $510 |
| 40k                     | 33.2k            | $553 | $692 | $830 | $968 |
| 80k                     | 60.6k            | $1010 | $1263 | $1515 | $1768 |
| 100k                    | 73.4k            | $1223 | $1529 | $1835 | $2141 |
| 120k                    | 85.8k            | $1430 | $1787 | $2145 | $2503 |
| 160k                    | 111.4k           | $1856 | $2320 | $2785 | $3249 |
| 200k                    | 137.3k           | $2288 | $2860 | $3433 | $4005 |

Next, I created a federal student loan variable with a consolidated (weighted average) interest rate of 6.8% ranging from a total principal (including any accumulated interest) of 10k-100k. Since we have already made payments for 5 years under the PSLF program, these payments also qualify for the IBR program, and thus, we only have 15 more years of payments in order to qualify for IBR plan student loan forgiveness. I thus set the total number of years for complete student loan repayment to 15 years, for all strategies considered (i.e., Strategy 0, 1 and 2).

## Strategy 0: Income-based Repayment Plan

 Similar to the monthly savings rates by household income illustrated in Table 2, I calculated the expected net worth of someone who put all of their monthly savings towards paying off their student loans (Figure 1a), often paying more than the minimum required payment under the Income-based Repayment Plan. Next, I calculated the net worth of someone who put all of their extra monthly savings towards the stock market with an assumed rate of return of 8% during a specified deferment period (Figure 1b). Importantly, extra monthly savings was calculated by subtracting the minimum required IBR payment (calculated based on their discretionary income) from a household's total monthly savings potential. The resulting net worth was calculated by taking the total accumulated wealth in the stock market and subtracting the sum of the remaining principal and accumulated interest on the student loans. As you can see, pushing as much monthly savings to the stock market increased one’s net worth at a faster pace than when monthly savings were applied aggressively towards loan payoff.

![Figure 1. For a household making $60,000 during a 3 year deferment period and $100,000 after graduation, accumulated net worth using the income-based repayment plan is plotted over a 15 year repayment period. First, net worth is estimated for when (a) the maximum amount of monthly savings was used for loan payoff. Second, for when (b) the maximum amount of monthly savings was invested in the stock market.](loan_strategy0.png)



## Strategy 1: Deferment + Refinancing Over 15 Years

Similar to the monthly savings rates by household income illustrated in Table 2, I calculated the expected net worth of someone who put all of their monthly savings towards paying off their student loans (Figure 2a). Next, I calculated the net worth of someone who put all of their monthly savings towards the stock market, with an assumed rate of return of 8% during a specified deferment period (Figure 2b). The resulting net worth was calculated by taking the total accumulated wealth in the stock market and subtracting the sum of the remaining principal and accumulated interest on the student loans. As you can see, pushing as much monthly savings to the stock market increased one’s net worth at a faster pace than when monthly savings were applied aggressively towards loan payoff.

![Figure 2. For a household making $60,000 during a 3 year deferment period and $100,000 after graduation, accumulated net worth after using a 3 year deferment period (while in school) to avoid making loan payments and then refinancing after graduation at 4.5%. First, net worth is estimated for when (a) the maximum amount of monthly savings was use for loan payoff. Second, for when (b) the maximum amount of monthly savings was invested in the stock market.](loan_strategy1.png)

## Strategy 2: Refinance Right Away (i.e., During Deferment Period)

When one refinances right away, they must begin making payments immediately, and thus they must be able to afford these payments while still in school. This eliminates debt refinancing over a certain amount of principal when one's monthly household income is too low to afford the required refinance monthly payment. Assuming we can afford the minimum refinancing monthly payments, refinancing right away would allow us to take advantage of the very low student loan refinance rates offered right now, but would seriously hamper our ability to contribute savings to tax advantaged retirement accounts and regular brokerage accounts while we are still in school.

Given the maximum monthly savings estimates in Table 2, I calculated the expected net worth of someone who put all of their maximum savings towards paying off their student loans, meeting or exceeding the minimum refinance payment required (Figure 3a). Next, I calculated the net worth of someone who put all of their maximum savings towards the stock market (with an assumed rate of return of 8%), minus the minimum monthly payment required by a student loan refinanced at 3% (Figure 3b). The resulting net worth is thus calculated by taking the total accumulated wealth in the stock market and subtracting the sum of the remaining principal and accumulated interest on the student loans. As we saw in Strategy 1, pushing one's monthly savings to the stock market increased one’s net worth faster than pushing monthly savings to loan payoff did. This conclusion is most strongly seen in the higher loan principal amounts (i.e., 50k and 100k).


![Figure 3. For a household making $60,000 during a 3 year deferment period and $100,000 after graduation, accumulated net worth after refinancing student loans right away at a 3% interest rate. First, net worth is estimated for when (a) the maximum amount of monthly savings was used for loan payoff. Second, for when (b) the maximum amount of monthly savings was invested in the stock market.](loan_strategy2.png)



## Conclusion: Contributing to the stock market outperforms aggressive loan payoff and refinancing right away was optimal.
We saw that for each strategy, sending as much of our projected monthly savings to the stock market as possible outperformed using monthly savings to pay off our student loan debt (Figures 1-3). When comparing each strategy head to head when monthly savings were sent to the stock market, refinancing right away (i.e., Strategy 2) outperformed both the federal government's income-based repayment (IBR) plan (i.e., Strategy 0) as well as Strategy 1, which involved delaying making payments while in school.

Below is an example plot for a typical married couple in graduate school with a household income of $60,000 for the remaining 3 years they are in school, followed by an increased household income of $100,000 upon graduation (based on the average American household income for married couples). Interest rates for refinancing was set to 3% for refinancing right away (Strategy 2) and 4.5% for refinancing in 3 years (Strategy 1), based on current refinancing rates offered today and the expected refinancing rates which should increase over the next few years. If you'd like to punch in your own numbers, visit visit [this Heroku app](https://lrt-amortization.herokuapp.com/) I created to see what the numbers say about your own situation.

![Figure 4. Comparison of building our net worth by allocating savings according to strategy 0 versus strategy 1 versus strategy 2. (a) Loan principal $10,000 (b) $50,000 (c) $100,000](strategy_compare.png)

When considering how each strategy affects one's net worth, it’s important to note that a final decision is actually more complicated than just a simple net worth calculation. You can have all your money tied up in the stock market but not be able to spend any of it without incurring capital gains taxation when you go to sell that equity. Furthermore, this analysis doesn’t take into consideration the emotional impact of carrying debt for most of one’s life - 15 years is a long time to carry an emotional burden like debt!

In the end, the emotional landscape of student loan debt and the taxes associated with selling investments may make taking a hybridized approach more appealing to many borrowers (i.e., refinance some of your loans right now, and use the deferment period for the remaining loan balance).
