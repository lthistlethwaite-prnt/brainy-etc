+++
author = "Lillian Thistlethwaite"
title = "Where Do the Recommended Weight Gain Guidelines for Pregnancy Come From?"
date = "2022-04-11"
description = "Examining weight gain guidelines for pregnancy."
featured = true
tags = [
    "epidemiology"
]
categories = [
    "analysis",
    "data science"
]
series = ["Analysis"]
thumbnail = "schedule_final.png"
+++

## Introduction
I recently gave birth to a healthy baby boy in February of 2022. As soon as I got that positive pregnancy test, the mom guilt began. At the time of my positive pregnancy test, I was 156 pounds (BMI: 25.6). Having been diagnosed with Poly-cystic ovarian syndrome (PCOS) just 4-5 months prior, I [drastically changed my diet](https://www.medicalnewstoday.com/articles/323002) and had lost about 20-25 pounds, in order to restore my ability to ovulate naturally. However, despite my successful weight loss, I was still entering pregnancy in the overweight category, and keeping the weight off meant continuing to be faithful to my new diet.

My diagnosis and combined positive pregnancy test paralyzed me in the beginning. The birth statistics for moms with PCOS are depressing. There is a [1.66x increased chance your baby will have autism](https://pubmed.ncbi.nlm.nih.gov/30867561/), a [3x increased chance you will miscarry](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3659904/), [2.44x increased chance for developing gestational diabetes](https://doi.org/10.2337/dc06-0877), [4x increased chance for developing pre-eclampsia](https://pubmed.ncbi.nlm.nih.gov/25356655/), and as a result of having a greater chance of having a large-for-gestational-age baby due to increased gestational diabetes risk, a [slight increased chance of having a C-section](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4880155/).

All throughout my pregnancy, I was given guidelines, rules to live by, and restrictions to follow, in order to protect the health of my growing fetus. Many of these rules made sense and were explained to me. However, some of the rules were on a "just trust me" basis and I began to feel frustrated by the lack of transparency from my doctors who clearly were not "top of mind" with the literature, or at least not particularly keen on sharing it with me. The biggest "just trust me" guideline I had the most issues with was the weight gain [guidelines for pregnant women](https://www.cdc.gov/reproductivehealth/maternalinfanthealth/pregnancy-weight-gain.htm). In general the weight gain guidelines were given based on one’s pre-pregnancy BMI, where underweight BMI women were told to gain more weight on average than normal BMI, overweight BMI and obese BMI women (Figure 1).

![Figure 1. Weight gain recommended based on pre-pregnancy BMI and gestational week in pregnancy.](schedule_final.png)

The CDC states that women who were overweight or obese were more likely to have pregnancies complicated by gestational diabetes, hypertension or pre-eclampsia, compared to women in the normal pre-pregnancy weight range. Similarly, women with an underweight BMI were more likely to have a baby who is “too small”, which is associated with developmental delays after birth. However, these facts just tell me that entering pregnancy overweight puts me at risk, not that the pounds gained during pregnancy contributes to that risk. Further, the specific guidelines wherein women with a normal pre-pregnancy BMI should gain 25-35 pounds, and overweight BMI should only gain 15-25 pounds, etc. was never justified to me, and I began to suspect these numbers were just “eyeballed” and not rigorously determined.

Upon a rigorous literature review, I finally found [the original 1990 study which set the guidelines](https://www.ncbi.nlm.nih.gov/books/NBK235227/), and [this series of papers](https://pubmed.ncbi.nlm.nih.gov/20669500/) which does a decent job at re-examining where these guidelines come from and their usefulness (see Table 1 for an overview). In particular, the authors point out that management of GDM includes dietary counseling and efforts to control weight gain, and therefore, that using total maternal weight gain rather than weight gain until the time of diagnosis would lead to poorly reproducible claims, depending on how faithful a given study's patients are to their prescribed dietary and exercise programs. Secondly, given that there is strong evidence for pre-pregnancy BMI on risk for several major bad outcomes in pregnancy, the greatest concern with postpartum weight retention is that a mom might move into a higher BMI category and thus accrue greater risk of pregnancy complications and adverse birth outcomes in a subsequent pregnancy. Given the moderate evidence for short term (<36 mos.) weight retention and that a large percentage of mothers who decide to have multiple children do so within 36 months of each other, setting a top bound on maternal weight gain during pregnancy (and after) does have some ground to stand on. Similarly, not gaining enough being associated with difficulty initiating lactation is further motivation for establishing a bottom bound on a weight gain range.

Table 1. Overview of findings in Weight Gain During Pregnancy: Reexamining the Guidelines, [Consequences of Gestational Weight Gain for the Mother](https://www.ncbi.nlm.nih.gov/books/NBK32818/).
| Outcome | Important Covariates | Final Ruling |
| ---- | ---- | ---- |
| Gestational Diabetes Mellitus (GDM) | Dietary counseling | Inconclusive |
| Gestational Hypertension/Pre-eclampsia | Maternal age/height/pre-pregnancy BMI, smoking status, gestational age, parity, 2 hour glucose tolerance test | Inconclusive |
| C-section | Route of delivery in previous pregnancies, GDM, pre-eclampsia | Moderate |
| Postpartum weight retention | Diet, Exercise, Breastfeeding behavior | Moderate (3-36 mos.), Weak (>36 mos.) |
| Initiation of Lactation | - | Moderate for low weight gain |

The actual weight gain guidelines, however, come from that original 1990 study of weight gain in women. Gestational gains at the 15th-85th percentile was 16-40 pounds at the time, which is similar to the guidelines we see today (15-40 pounds). The main concern of the 1990 study seems to be to identify rates of weight gain at each trimester, where the second and third trimesters result in about 0.7-1.4 pound gains per week. Rates above and below that rate are considered abnormal and flagged for intervention.

## Challenges with My Analysis of the CDC's 2019 Birth Data
In order to validate some of the findings outlined in the literature, I downloaded [birth data from the CDC](https://data.nber.org/natality/2019/raw_files/nat2019us/Nat2019PublicUS.c20200506.r20200915.txt) to see if I could identify similar findings. Specifically, I looked at the data collected from the year 2019 which was described [here](https://ftp.cdc.gov/pub/Health_Statistics/NCHS/Dataset_Documentation/DVS/natality/UserGuide2019-508.pdf).

The CDC birth data takes statistics from moms throughout their pregnancy all across the U.S. When aggregated, you end up having millions of data points for every statistic tracked. The 2019 CDC birth data has just shy of 4 million data points for singleton pregnancies alone. This is one of the reasons I didn't bother getting data from more than one year, as I wasn't looking to capture a historical perspective on the weight gain guidelines, and there is enough data to go the moon and back for the questions I was interested in, in just one year's worth of mamas.

When you're looking at basic statistics, like case-control comparisons for various facets in the birth data, the sheer amount of data can drive a significant result. However, rushing to the presses to declare an effect is a huge mistake, because statistical significance isn't the goal. The goal is finding significant differences between groups that also show a clinically "large enough" effect size. Beyond documenting what findings I could reproduce about what is known regarding weight gain during pregnancy, I also want to document how easily the data can fool you if you're not careful, or sloppy, with your analysis.


## Weight Gain By Pre-Pregnancy BMI
The first stop in my analysis was just to do some observational snapshots of the data. What does the distribution of weight gain look like for each BMI class? Upon inspection, I found very similar percentiles for gestational weight gain reported for each BMI class (Figure 2).

![Figure 2. Full term percentile weight gain ranges based on pre-pregnancy BMI.](percentiles_weight_gain.png)

Furthermore, the CDC reports that only [about a third of women stay within the recommended weight gain guidelines](https://www.cdc.gov/reproductivehealth/maternalinfanthealth/pregnancy-weight-gain.htm). The 2019 CDC birth data generally replicates this finding, with about 31.3% of women staying within the guidelines for their pre-pregnancy BMI weight class. Beyond a view of overall compliance, I looked at compliance by pre-pregnancy BMI class. Moms starting out underweight had the highest compliance (42%), which makes sense because they are given a larger allowable weight gain range compared to all other BMI classes. This "compliance vs. magnitude of the weight range restriction" trend held for all other BMI classes (Normal: 38% compliant, Overweight: 27% compliant, Obese: 24% compliant), where a smaller allowable weight gain resulted in lower compliance. Nothing shocking to report here.

## Pre-Pregnancy BMI Contributed to Risk for Several Bad Outcomes, Excess Maternal Weight Gain Lowered Risk
I then looked at basic relationships between gestational weight gain and bad outcomes associated with either the mother or the child. Some examples of bad outcomes reported in the CDC birth data are outlined in Table 2 below.

Table 2. Bad outcomes reported in the 2019 CDC birth data.
| Affected | Outcomes |
| ---      | ---      |
| Mother | Gestational diabetes, Gestational hypertension, Eclampsia, C-section, Ruptured Uterus, Newborn size (SGA or LGA) |
| Child | Low newborn Apgar score, Needed ventilation, Was in NICU, Seizures, Anencephaly, Spinabifida, Congenital heart disease, Cogenital diaphragmatic hernia, Omphalocele, Gastroschisis, Limb defect, Hypospadias|

The first thing I tried was a basic comparison of group means for weight gain between cases (did have the bad outcome) and controls (did not have the bad outcome). Take, for example, gestational diabetes (GD) in moms. An average mom without gestational diabetes gained 29.23 pounds in 2019, and an average mom with GD gained only 24.13 pounds, an effect with the opposite direction expected based on what we know about GD from the literature (that people will diabetes have a harder time controlling weight gain). The p-value on this difference is extremely significant and Cohen's d reveals a small effect size (0.33), but as I foreshadowed previously, this p-value is absolutely driven by the sheer amount of data we have available for this statistic (~3.5 million data points). If you subsampled the data to a smaller number of data points per BMI class, the significance shrinks, whereas all other variables stay the same (Table 3).

Table 3. Mean statistics across 100 trials of subsampling for a variable number of data points per BMI class, compared to the same statistics on the full dataset.
| # Data Points per BMI Class | P-value | Cohen's d | Mean WG Control | Mean WG with GD |
| ---- | ---- | ---- | ---- | ---- |
| 100  | 0.214 | 0.346 | 29.753 +/- 15.06 | 24.597 +/- 15.30 |
| 250  | 0.077 | 0.329 | 29.839 +/- 15.04 | 24.833 +/- 15.99 |
| 500  | 0.013 | 0.336 | 29.834 +/- 15.01 | 24.632 +/- 15.83 |
| 1000  | 0.000 | 0.333 | 29.787 +/- 15.03 | 24.635 +/- 15.90 |
| 10000  | 0.000 | 0.329 | 29.793 +/- 15.05 | 24.662 +/- 16.03 |
| Full | 0.000 | 0.326 | 29.23 +/- 15.26 | 24.13 +/- 16.04 |

What's even more underwhelming are the large standard deviations around the means for each group. A 15-16 pound deviation around means in the 24-29 range is brutal: that's more than half each means' value! This should absolutely shake our confidence in predicting whether or not, say, a mom with a 24 pound weight gain will develop GD. Even further troubling is the fact that the direction of the observed differences in the means is contrary to what I expected. Based on what is known about diabetes, GD moms should have more difficulty keeping weight controlled. Could a third variable explain or at least partially explain this effect, such as pre-pregnancy BMI class? Perhaps being told by the doctor not to gain more weight for overweight and obese women predicted that they in fact gained less weight. A basic logistic regression model predicting case (y=1) control (y=0) status for GD, with maternal weight gain (e.g., `delta_weight`), and pre-pregnancy BMI class (e.g., `pre_bmi_cat`) as predictive covariates revealed the output shown below.

```
Logit Regression Results                           
==============================================================================
Dep. Variable:                      y   No. Observations:              3468232
Model:                          Logit   Df Residuals:                  3468229
Method:                           MLE   Df Model:                            2
Date:                Tue, 06 Sep 2022   Pseudo R-squ.:                 0.03286
Time:                        16:14:09   Log-Likelihood:            -8.3775e+05
converged:                       True   LL-Null:                   -8.6621e+05
Covariance Type:            nonrobust   LLR p-value:                     0.000
================================================================================
coef    std err          z      P>|z|      [0.025      0.975]
--------------------------------------------------------------------------------
intercept       -3.6819      0.006   -631.392      0.000      -3.693      -3.670
pre_bmi_cat      0.5659      0.003    225.978      0.000       0.561       0.571
delta_weight    -0.0151      0.000    -84.497      0.000      -0.015      -0.015
================================================================================
```

There are a few things that jump out here. The significance for each coefficient is very significant and the standard error is tight. This makes sense because we have ~3.5 million data points available to estimate the coefficient for each of our independent variables' effect on gestational diabetes status.

Next, it is exceptionally tempting to worry about the "Pseudo R-squared" value. I admit I got hung up on this for a bit. The pseudo R2 is the analog in logistic regression to the R2 value in linear regression models, where R2 describes how much of the variation in y is explained by the independent variables found in x, where y ~ B_i*x_i + b, for all variables i in x, and where b is the model intercept. Typically, an R2 or pseudo R2 of 1.0 is expected to be a highly performant model, whereas a model with a low (pseudo-) R2 value will not have strong predictive accuracy and more variables should be added to improve the percent of explained variation in y. We see here that our two variable model has a 0.032 pseudo R2. By this logic, our model is awful and if we were interested in predicting gestational diabetes status, we would need to continue to explore new features to add to our model to improve our model's fit with y. However, it's important to note that data with high levels of noise will result in a low R2 despite the model fitting the relationships (linearity, slope/direction, no bias) between x and y perfectly (see [this post](https://data.library.virginia.edu/is-r-squared-useless/), and Figure 3 below).

![Figure 3. R2 tanks as the model's residual error increases.](residual_error.png)

One could argue that the noise in y unexplained by the model is the result of not including more features that can explain that noise, and this isn't wrong, but the point is for data in some domains, the missing features that would explain the residual error can be difficult to suss out. However, if you have a low R-squared value but the independent variables show statistically significant coefficients, you can still draw important conclusions about the relationships between the variables.

So bearing what we've just discussed about the pseudo R2 value, the most important thing to look at are the coefficient values themselves. Given that our model is very confident in their estimation of each coefficient, we can directly interpret their values by converting each coefficient to an interpretable metric. For the intercept, we would prefer to report this as a "baseline" probability of developing gestational diabetes. You can calculate this by running the intercept's coefficient through the sigmoid function: e^x/(1+e^x), or e^-3.6819/(1+e^-3.6819) = 2.46%. The CDC reports a prevalence for gestational diabetes between 2-10% per year, which is consistent with our model's reported baseline probability. For each remaining coefficients, you can convert them to an odds ratio from a log odds ratio by exponentiating each coefficient. This gives e^0.5659=1.761, or a 76% increased chance of developing gestational diabetes for each BMI class increase; and e^-0.0151=0.985, or a 1.5% decreased chance of developing gestational diabetes for every excess pound gained beyond the recommended guidelines.

When we recall that Obese people tend to gain less than Underweight and Normal pre-pregnancy BMI moms (Figure 2), our comparison of group means in Table 3 begins to make sense given the positive association between developing gestational diabetes and pre-pregnancy BMI. Because the likelihood you'll get GD increases, and the likelihood you'll gain less weight as your pre-pregnancy BMI increases, it makes sense that moms with gestational diabetes on average gain less weight than moms without gestational diabetes - because they tend to be overweight or obese! This can be further confirmed by visualizing 1000 random samples per BMI class in a "jittered" scatter plot (Figure 4). The random sampling is key so you don't oversample in one part of the weight gain distribution for each BMI class.

![Figure 4. 1000 random samples per BMI class for maternal weight gain.](1000_weight_gain.png)

You can approximate the 76% increase expected per BMI class jump by simply calculating what percentage of moms in each pre-pregnancy weight class developed GD (Table 4). As you can see, the increase in percentage increases by an average of 2.69% per BMI class jump. If you calculate the average fold increase per BMI class jump you get: ((3.29/2.46 + 4.05/3.29 + 6.70/4.05 + 11.37/6.70) / 4) - 1.0 = 52.7% increase. This is slightly lower than 76% because the negative effect of `delta_weight` seems to be pushing the estimate for `pre_bmi_cat` towards a stronger positive estimate.

Table 4. Percentage of moms with gestational diabetes (GD) by pre-pregnancy BMI class.
| Pre-pregnancy BMI Class | % Developed GD |
| ---- | ---- |
| Underweight | 3.29% |
| Normal |  4.05% |
| Overweight |  6.70% |
| Obese | 11.37% |

I repeated the same kinds of comparisons for all bad outcomes reported in the CDC birth data. Table 5 shows that similar findings across the board existed: increases in maternal weight gain beyond the guidelines had small and mostly *protective* effects on several bad outcomes. Overall, maternal weight gain exceeding the guideline ranges is not considered particularly impactful on risk for bad outcomes, across the board. On the other hand, pre-pregnancy BMI class was strongly predictive of developing several bad outcomes, particularly for the mom (Table 5).

Table 5. Relationships between bad outcomes for mom and child and maternal weight gain at delivery, versus pre-pregnancy BMI.
| Outcome | Baseline Odds | Increased Risk per Excess Pound Gained During Pregnancy | Increased Risk per Pre-pregnancy BMI Class Jump |
| --- | --- | --- | --- |
| Gestational diabetes | 2.8% | -1.5% | 76.1% |
| Gestational hypertension | 3.2% | 1.8% | 60.1% |
| Eclampsia | 0.1% | 1.1% | 45.1% |
| C-section | 28.4% | 0.9% | 41.6% |
| Large for gestational age | 4.9% | 3.2% | 27.4% |
| Ruptured uterus | 0.03% | -1.3% | 14.8% |
| Gastroschisis | 0.05% | -0.7% | -36.2% |
| Spinabifida | 0.008% | -1.0% | 34.6% |
| Low Apgar score | 0.3% | -3.1% | 28.0% |
| Needed ventilation | 3.7% | -0.3% | 19.8% |
| Was in NICU | 7.9% | -0.8% | 17.9% |
| Anencephaly | 0.007% | -3.4% | 15.6% |
| Omphalocele | 0.007% | -0.2% | 18.1% |
| Seizures | 0.03% | 0.2% | 10.0% |
| Congenital heart disease | 0.05% | -0.4% | 7.5% |
| Limb defect | 0.01% | -0.8% | 5.0% |
| Hypospadias | 0.05% | 0.8% | 2.2% |
| Congenital diaphragmatic hernia | 0.01% | -0.7% | 1.4% |




## Maternal Weight Gain During Pregnancy Not Reasonably Associated with Newborn Weight at Delivery
Lastly, I looked at a basic relationship between gestational weight gain and newborn weight and sex at delivery. For every extra pound gained during pregnancy, the newborn's weight was on average 0.018 pounds heavier, with a mean weight of 6.82 pounds and newborn weight and pre-pregnancy BMI as covariates.

```
OLS Regression Results                            
==============================================================================
Dep. Variable:                      y   R-squared:                       0.058
Model:                            OLS   Adj. R-squared:                  0.058
Method:                 Least Squares   F-statistic:                 7.213e+04
Date:                Tue, 06 Sep 2022   Prob (F-statistic):               0.00
Time:                        20:02:56   Log-Likelihood:            -5.8058e+06
No. Observations:             3519776   AIC:                         1.161e+07
Df Residuals:                 3519772   BIC:                         1.161e+07
Df Model:                           3                                         
Covariance Type:            nonrobust                                         
================================================================================
coef    std err          t      P>|t|      [0.025      0.975]
--------------------------------------------------------------------------------
intercept        6.8244      0.002   2912.624      0.000       6.820       6.829
delta_weight     0.0180   4.51e-05    399.832      0.000       0.018       0.018
newborn_sex      0.2381      0.001    177.274      0.000       0.236       0.241
pre_bmi_cat      0.1879      0.001    240.622      0.000       0.186       0.189
==============================================================================
Omnibus:                   474954.094   Durbin-Watson:                   1.968
Prob(Omnibus):                  0.000   Jarque-Bera (JB):          4973568.753
Skew:                          -0.282   Prob(JB):                         0.00
Kurtosis:                       8.796   Cond. No.                         120.
==============================================================================
```

So for an overweight mom that gained 40 pounds instead of 20 pounds, her baby boy would be expected to be 6.82+0.018(40)+0.2381+0.1879(2)=8.15 pounds instead of 6.82+0.018(20)+0.2381+0.1879(2)=7.80 pounds. In the grand scheme of things, jumping from 30 to 40 pounds gained is a major jump in the maternal weight gain distribution, but the effect on the newborn's weight is minimal. Clearly maternal weight gain is not a major player in predicting newborn weight at delivery.
