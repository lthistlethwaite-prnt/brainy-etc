+++
author = "Lillian Thistlethwaite"
title = "How to Use the Jiant Library for Multi-task Learning"
date = "2022-01-04"
description = "In depth guide of how to perform multi-task learning with the jiant python library."
featured = true
tags = [
    "python",
    "deep learning",
    "featured"
]
categories = [
    "repositories",
    "how-to",
]
series = ["How-To"]
thumbnail = "multitasking.png"
+++


## Introduction

The [jiant python library](https://github.com/nyu-mll/jiant) is a GIANT python library that facilitates multi-task learning. The library can feel complex and difficult to use at first, in part because it confounds the concepts of "tasks" (e.g., sentiment analysis, named entity recognition, event extraction, reading comprehension, etc.) with the "datasets" (e.g., RTE, MNLI, STSB, MRPC, etc.) that embody a task. For example, for sentiment analysis, the task "sst" (Stanford Sentiment Treebank 2) is intimately tied to the underlying dataset it uses to make binary classifications on review outcome ("positive" vs. "negative") of movie reviews. The "sst" task does not support multi-class classifications, and so if you want to do sentiment analysis on more than 2 classes - and sometimes on a different dataset if you're running multiple tasks from the same task type (i.e., multi-instance vs. multi-task learning) - you need to create your own "task" that works with your private dataset(s). In this document, I will walk you through what you need to do to create your own task in jiant.


## How to Add Your Own Task {#add_task}
The original documentation about how to add your own task from jiant can be found [here](https://github.com/nyu-mll/jiant-v1-legacy/blob/master/tutorials/adding_tasks.md).

First off, you don't have to register your task, you can just skip the @register_task() and hack under the hood to get the functionality you need without exposing your private data to the jiant registry. Secondly, the documentation at the link above is a bit outdated, so I would recommend leaning more on the following instructions instead.

### Step One: Copy & modify an existing task class file located in /jiant/tasks/lib/*.py. {#copy-class-file}
For this example, I will copy the sst.py file and make a sentiment2.py and a sentiment4.py file, which allows me to create a binary classification task, and a 4-class classification task, respectively, for my own private datasets. Here are the main lines you need to modify in sentiment2.py and sentiment4.py modeled after sst.py.

Since our private dataset is not meant to be submitted to the GLUE benchmark, delete the import to the GlueMixin class (defined in /jiant/tasks/core.py)

```
    from jiant.tasks.core import (
        BaseExample,
        BaseTokenizedExample,
        BaseDataRow,
        BatchMixin,
        ~~GlueMixin,~~
        Task,
        TaskTypes,
    )
```

Next, modify the Example.tokenize() function to point to the correct task class. We will name our sentiment2 class Sa2Task and sentiment4 class Sa4Task. Below is the new tokenize() function for the Sa2Task class.

```
    def tokenize(self, tokenizer):
        return TokenizedExample(
            guid=self.guid,
            text=tokenizer.tokenize(self.text),
            ~~label_id=SstTask.LABEL_TO_ID[self.label],~~
            label_id=Sa2Task.LABEL_TO_ID[self.label],
        )
```

Next, change the actual task declaration name from SstTask to Sa2Task or Sa4Task. Also remove the GlueMixin class reference.

```
    ~~class SstTask(GlueMixin, Task):~~
    class Sa2Task(Task):
```

Lastly, change the LABELS list to point to the labels defined in your .jsonl files found in /content/tasks/data/sentiment2 project folder. Make sure the data types also match. If your labels are strings, you should set them to the same strings in your "label" property of your .jsonl elements. If your labels are integers, as mine were, you need to set the LABELS list to be integer classes.

```
    # For string class names
    LABELS = ["Class1", "Class2"]

    # If you have integer classes
    LABELS = [0, 1]

    # This will break the code if you have integer classes in the
    # "label" property of your .jsonl files
    LABELS = ["0", "1"]
```

The labels_to_bimap() function essentially maps string class names to integers anyway, but the code only works if LABELS is set to the correct data type.


### Step Two: Add new tasks to /jiant/tasks/evaluate/core.py. {#evaluate/core.py}
In get_evaluation_scheme_for_task(), add Sa2Task and Sa4Task to the isinstance() function pointing to the appropriate evaluation approach you'd prefer. The default for most tasks is SimpleAccuracyEvaluationScheme(), but there are a few others that might be more desirable for your task, particularly if you have imbalanced data. Some alternatives to consider are

    SimpleAccuracyEvaluationScheme()     - line 260
    AccAndF1EvaluationScheme()           - line 336
    MCCEvaluationScheme()                - line 355
    PearsonAndSpearmanEvaluationScheme() - line 366

You can also write your own custom evaluation scheme. For for information, see the [How to Hack Evaluation Metric Objectives](#how-to-hack-evaluation-metric-objectives) section below.

### Step Three: Modify /jiant/tasks/retrieval.py. {#retrieval.py}
Add an import to your new task class in retrieval.py.

```
    from jiant.tasks.lib.sentiment2 import Sa2Task
    from jiant.tasks.lib.sentiment4 import Sa4Task
```

Then, add both tasks to the TASK_DICT

```
    TASK_DICT = {
      ...,
      "sst": SstTask,
      "sentiment2": Sa2Task,
      "sentiment4": Sa4Task,
      ...,
    }
```


## How to Hack Evaluation Metric Objectives
For your task, you may have an imbalanced dataset, where there are
more observations for class 1 than class 2. In this case, maximizing
accuracy as your objective will result in a model that votes for the majority class no matter what the input sequence text is.

Instead, you might want to maximize the Precision-Recall AUC (for binary classification) or F1, or some other alternative evaluation
metric. You can do this by modifying which evaluation.

You can also write your own custom evaluation scheme. I wrote one for the Precision-Recall AUC for binary classification tasks.

```
    # Works only for binary classification
    class PRAucEvaluationScheme(BaseLogitsEvaluationScheme):
    def get_preds_from_accumulator(self, task, accumulator):
        logits = accumulator.get_accumulated()
        return np.argmax(logits, axis=1)

    @classmethod
    def compute_metrics_from_preds_and_labels(cls, preds, labels):
        acc = float((preds == labels).mean())
        labels = np.array(labels)
        f1 = f1_score(y_true=labels, y_pred=preds, average="macro")
        precision, recall, _, _ = precision_recall_fscore_support(y_true=labels, y_pred=preds, average="macro")
        precs, recs, threshs = precision_recall_curve(y_true=labels, probas_pred=preds)
        pr_auc = auc(precs, recs)
        minor = {
            "pr_auc": pr_auc,
            "acc": acc,
            "precision": precision,
            "recall": recall,
            "f1": f1,
        }
        return Metrics(major=minor["pr_auc"], minor=minor)
```

## How to Hack the Metric Aggregator Config
Navigate to /jiant/proj/main/scripts/configurator.py. As a default, both the SingleTaskConfigurator and the SimpleAPIMultiTaskConfigurator classes use the EqualMetricAggregator class. However, there are others available, and you can always write your own. For single task models, the aggregator class doesn't matter because there is only 1 task being evaluated. However, for multi-task models, you may care more about Task 1's performance than Task 2


## How to Hack the Configuration Sampling
It might be that Task 2 has a lot more data than Task 1 and you worry the batch sampling during training will favor the task with more data. In fact, the default sampling approach is to use the ProportionalMultiTaskSampler class, which does sample from tasks more frequently that have more data. However, there are other alternative sampling approaches available to you, and you can always write your own. The other alternative currently included in jiant is the SpecifiedProbMultiTaskSampler class.

To use the SpecifiedProbMultiTaskSampler, first navigate to the /jiant/proj/main/scripts/configurator.py. In this file, you'll find in the SimpleAPIMultiTaskConfigurator class that there is an optional parameter called train_examples_cap which you can set when you run an experiment in jiant. When you use this parameter, you should set it to the number of samples you want each task to sample per batch during training. Setting this parameter triggers the use of the SpecifiedProbMultiTaskSampler instead of the default ProportionalMultiTaskSampler.


## How to Get Predictions from the Test Data Partition with jiant
Setting write_test_preds = True in your main_runscript.RunConfiguration() call triggers the code to write predictions for test data partition to "test_preds.p" in the same directory your model is saved if you set do_save=True (see [below](#how-to-save-reload-a-model-trained-by-jiant)). This will save a dictionary with predictions, "preds", and "guids" as keys. However, you'll get an empty dictionary if you don't set test_task_name_list=TASKS like you did for train and val in the configurator.SimpleAPIMultiTaskConfigurator() call.

```
    jiant_run_config = configurator.SimpleAPIMultiTaskConfigurator(
        task_config_base_path=f"{EXPERIMENT_DIRECTORY}/tasks/configs",
        task_cache_base_path=f"{EXPERIMENT_DIRECTORY}/cache",
        train_task_name_list=TASKS, val_task_name_list=TASKS,
        **test_task_name_list=TASKS,**
        train_batch_size=4, eval_batch_size=8,
        epochs=0.5, num_gpus=1).create_config()

    run_args = main_runscript.RunConfiguration(
        jiant_task_container_config_path=f"{EXPERIMENT_DIRECTORY}/run_configs/jiant_run_config.json",
        output_dir=f"{EXPERIMENT_DIRECTORY}/runs/jiant_main3_bal",
        hf_pretrained_model_name_or_path="roberta-base",
        model_path=f"{EXPERIMENT_DIRECTORY}/models/roberta-base/model/model.p",
        model_config_path=f"{EXPERIMENT_DIRECTORY}/models/roberta-base/model/config.json",
        learning_rate=1e-5, eval_every_steps=500, do_train=True, do_val=True,
        **do_save=True,** force_overwrite=True)
```

Once you set this parameter, you'll get some errors about missing files in the cache directory. To work around this jiant bug, you need to do the following.

```
    mkdir content/cache/sentiment2/test
    cp content/cache/sentiment2/train/data_chunk0000 content/cache/sentiment2/test/
    cp content/cache/sentiment2/train/data_args.p content/cache/sentiment2/test/
    mkdir content/cache/sentiment4/test
    cp content/cache/sentiment4/train/data_chunk0000 content/cache/sentiment4/test/
    cp content/cache/sentiment4/train/data_args.p content/cache/sentiment4/test/
```

Then run your experiment and verify the resulting "test_preds.p" was written to your output_dir.

```
    main_runscript.run_loop(run_args)
```

![Output files](jiant_test_preds_verify.png)

## How to Save & Re-load a Model Trained by jiant
Importantly, the code in the README.md of the jiant Github repository doesn't include important flags you might want to use if you intend to reuse a trained multi-task model. For example, if you want to save the model after training, you need to set the do_save to True in the RunConfiguration() function.

```
    run_args = main_runscript.RunConfiguration(
        jiant_task_container_config_path=f"{EXPERIMENT_DIRECTORY}/run_configs/jiant_run_config.json",
        output_dir=f"{EXPERIMENT_DIRECTORY}/runs/jiant_main3_bal",
        hf_pretrained_model_name_or_path="roberta-base",
        model_path=f"{EXPERIMENT_DIRECTORY}/models/roberta-base/model/model.p",
        model_config_path=f"{EXPERIMENT_DIRECTORY}/models/roberta-base/model/config.json",
        learning_rate=1e-5, eval_every_steps=500, do_save=True,
        do_train=True, do_val=True, force_overwrite=True)
    main_runscript.run_loop(run_args)
```

This flag triggers a PyTorch save in /jiant/shared/runner.py,  using:

```
    torch.save(model.state_dict(), path)
```

This saves *-model.p files in the specified path. To load a saved model using jiant, you can use PyTorch's torch.load("path/to/model.p") function. There's a couple of "gotchas" with using this function. First off, you have to initalize an empty model with the same architecture as your saved checkpoint, *-model.p. This means you need to understand the JiantModel class a bit before you torch.load() successfully. Check out the section How to Hack the JiantModel Class [below](#how-to-hack-the-jiant-model-class). In general, you can initialize a JiantModel object as below:

```
    EXPERIMENT_DIRECTORY="./content"

    jiant_task_container = container_setup.create_jiant_task_container_from_json(
          jiant_task_container_config_path=f"{EXPERIMENT_DIRECTORY}/run_configs/jiant_run_config.json", verbose=True)
    jiant_model = jiant_model_setup.setup_jiant_model(
          hf_pretrained_model_name_or_path="roberta-base",
          model_config_path=f"{EXPERIMENT_DIRECTORY}/models/roberta-base/model/config.json",
          task_dict=jiant_task_container.task_dict)
```

Then, load your checkpoint saved in *-model.p with torch.load()

```
    RUN_NAME = "jiant_main3_bal"  # output_dir path parameter you specified when you ran main_runscript.RunConfiguration() initially.
    checkpoint = torch.load(f"{EXPERIMENT_DIRECTORY}/runs/{RUN_NAME}/best_model.p")
    jiant_model.load_state_dict(checkpoint)
    jiant_model.eval()
```

Instead of run_loop evaluating on the evaluation data partition (/content/tasks/data/sentiment2/*-val.jsonl) as you did during training, you can set the data that gets evaluated on to the test data partition in your /content/tasks/data/sentiment2/*_test.jsonl file. First, set do_train to False, since you already trained.



## Hack the JiantModel class
Inherently, the Jiant Model class builds the multi-task model starting from a pre-trained transformer model which the encoder is borrowed from and frozen. Below is a schematic view of the JiantModel structure, where the JiantModel.encoder is taken from the pre-trained transformer model, and the JiantModel.taskmodels_dict is comprised of all Task encoder+task head combinations for all defined tasks.

```
    JiantModel(
      (encoder): RobertaModel(
        (embeddings): RobertaEmbeddings()
        (encoder): RobertaEncoder(
          (layer): ModuleList(
            (0-11): RobertaLayer(
              (attention): RobertaAttention()
              (intermediate): RobertaIntermediate()
              (output): RobertaOutput())))
        (pooler): RobertaPooler(
          (dense): Linear(in_features=768, out_features=768, bias=True)
          (activation): Tanh())
      )
      (taskmodels_dict): ModuleDict(
        (sentiment4): ClassificationModel(
          (encoder): RobertaModel(
            (embeddings): RobertaEmbeddings()
            (encoder): RobertaEncoder((layer): ModuleList((0-11): RobertaLayer()))
            (pooler): RobertaPooler()
          )
          (head): ClassificationHead()
        )
        (sentiment2): ClassificationModel(
          (encoder): RobertaModel(
            (embeddings): RobertaEmbeddings()
            (encoder): RobertaEncoder((layer): ModuleList((0-11): RobertaLayer()))
            (pooler): RobertaPooler()
          )
          (head): ClassificationHead()
        )
      )
    )
```

It took me a while to verify that loading a model checkpoint was indeed loading different weights as from the initialized weights as seen in [](#how-to-save-a-model-trained-by-jiant). Here's some of the things I did to check.

```
    # First print all "named_parameters" in the model after loading the checkpoint.
    for f in jiant_model.named_parameters():
      print(f)
```

This allowed me to highlight specific properties inside the JiantModel.taskmodels_dict that I could print out to verify the weights had changed between the initialized model and the model with the load_state_dict() from the checkpoint applied.
