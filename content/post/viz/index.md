+++
author = "Lillian Thistlethwaite"
title = "Which Python viz library is the best, by plot type?"
date = "2022-11-22"
description = "Comparing Python Visualization libraries"
featured = true
tags = [
    "visualization", "app", "python"
]
categories = [
    "analysis",
    "data science"
]
series = ["Intro-to"]
thumbnail = "lrt-viz.png"
+++

## Introduction

I created a (Streamlit application)[http://lrt-viz.herokuapp.com/] to help visualize and provide code snippets for 13 common plot types, and 7 popular Python implementations of visualization libraries. The goal for this effort was to become familiar with the various plotting libraries available in Python, and to decide which one I preferred, by plot type. I chose to visualize data from the popular "tips" dataset, because it contains both numeric and categorical data types, and was readily available for use via seaborn's load_dataset() function. Below is a summary of my findings for each plot type.

## Scatter

![Scatter plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Scatter.png)

## Bar

![Bar plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Bar.png)

## Line

![Line plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Line.png)

## Histogram

![Histogram plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Histogram.png)

## Boxplots

![Box plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Boxplot.png)

## Violin

![Violin plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Violin.png)

## Pie

![Pie plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Pie.png)

## Heatmap

![Heatmap plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Heatmap.png)

## Maps

![Map plots from several popular Python viz libraries.](viz_gallery_post_Maps.png)

## Timeline

![Time plots from several popular Python viz libraries.](viz_gallery_post_Timeline.png)

## Regression

![Regression plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Regression.png)

## Pairwise

![Pairwise plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_Pairwise.png)


## Parallel Coordinates

![Parallel coordinates plots from the "tips" dataset from several popular Python viz libraries.](viz_gallery_post_ParallelCoords.png)
