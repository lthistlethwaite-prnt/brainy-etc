+++
author = "Lillian Thistlethwaite"
title = "Why are some spacy models slower than others?"
date = "2022-08-29"
description = "Comparing Named Entity Linking Spacy models"
featured = true
tags = [
    "spacy"
]
categories = [
    "analysis",
    "data science"
]
series = ["Analysis"]
thumbnail = "spacy.png"
+++

## Introduction

- Look at embedding size ("en_core_web_lg" vs "en_core_web_sm" based models)
- Look at speed at inference
- Look at # of entities mapped to a knowledge base id

- [Spacy Open Tapioca](https://github.com/UB-Mannheim/spacyopentapioca)
- [Spacy Fishing](https://github.com/Lucaterre/spacyfishing)
- [Spacy Dbpedia Spotlight](https://github.com/MartinoMensio/spacy-dbpedia-spotlight)
- [Facebook's GENRE](https://github.com/facebookresearch/GENRE)
