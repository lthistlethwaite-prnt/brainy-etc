+++
author = "Lillian Thistlethwaite"
title = "Ray vs. Optuna: Pros and Cons"
date = "2022-01-01"
description = "Weighing the pros and cons between Ray and Optuna."
tags = [
    "python",
    "deep learning"
]
categories = [
    "python",
    "how-to",
]
series = ["How-To"]
thumbnail = "images/ray-vs-optuna.png"
+++

## Introduction to Ray


## Introduction to Optuna
