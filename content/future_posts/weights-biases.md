+++
author = "Lillian Thistlethwaite"
title = "Introduction to Weights & Biases"
date = "2022-01-01"
description = "How weights and biases can help with your data science workflows."
tags = [
    "visualization"
]
categories = [
    "intro-to",
]
series = ["Intro-To"]
thumbnail = "images/weights-biases.png"
+++

## Introduction to Weights & Biases
