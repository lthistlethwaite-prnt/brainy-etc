+++
author = "Lillian Thistlethwaite"
title = "Introduction to Natural Language Processing (NLP)"
date = "2022-06-01"
description = "How to integrate hyperparameter tuning with Ray-Tune with HuggingFace's Transformers library and MLFlow's model tracking."
tags = [
    "python",
    "nlp",
    "deep learning"
]
categories = [
    "python",
    "intro-to",
]
series = ["Intro-To"]
thumbnail = "images/nlp.png"
+++

###

https://github.com/nyu-mll/jiant/blob/master/guides/tasks/supported_tasks.md

#### Question & Answer, Multiple Choice
- mctaco - Multiple Choice TemporAl COmmonsense
- boolq - Question Answering for yes/no questions
- arc_easy - AI2 Reasoning Challenge, science exam questions, multiple choice
- arc_challenge - AI2 Reasoning Challenge, science exam questions, multiple choice
- mctest500 - Multi-stage Multi-Task Learning for Multi-choice Reading Comprehension
- mctest160 - Multi-stage Multi-Task Learning for Multi-choice Reading Comprehension
- wsc - Winograd Schema Challenge, multiple choice test
- winogrande - Fill in the blank task, binary options

#### Question & Answer, Reading Comprehension
- multirc - Multi-sentence Reading Comprehension
- cosmosqa
- qnli
- piqa
- tydiqa
- newsqa
- ropes
- quoref
- quail - Question Answering for Artificial Intelligence
- qamr - Question Answer Meaning Representations
- squad_v1 - Stanford Question Answering Dataset
- squad_v2 - Stanford Question Answering Dataset
- qqp - Quora Question Pairs Dataset
- xquad - Cross Lingual Q&A Dataset
- qasrl - Question Answer driven SRL for Nominalizations
- mlqa - Multiway (cross-lingual) aligned extractive question answering
- commonsenseqa
- socialiqa
- mrqa_natural_questions - Machine Reading for Question Answering
- copa - Choice of Plausible Alternatives (commonsense reasoning)
- wnli
- arct - Argument Reasoning Comprehension Task
- wic - Word in Context Dataset
- record - Reading Comprehension with Commonsense Reasoning Dataset
- race_high - variant of race?
- race_middle - variant of race?
- race - Reading Comprehension Dataset
- mutual - Multi-Turn Dialogue Reasoning
- mutual_plus
- superglue_broadcoverage_diagnostics
- superglue_winogender_diagnostics
- glue - General Language Understanding Evaluation benchmark
- glue_diagnostics

#### Sentiment Analysis
- sst

#### Named Entity Recognition
- panx - aka Wikiann
- udpos - identifies words as parts of speech (POS)

#### Text Similarity
- stsb
- wic - Word in Context Dataset
- senteval_top_constituents
- senteval_coordination_inversion
- senteval_bigram_shift
- senteval_obj_number
- senteval_subj_number
- senteval_tree_depth
- senteval_word_content
- senteval_past_present
- senteval_odd_man_out
- senteval_sentence_length

#### Paraphrase
- mrpc
- pawsx - Paraphrase Adversaries from Word Scrambling



#### Finish Your Sentence (Text Generation?)
hellaswag
swag

#### Text Entailment (Sentence Pairs)
mnli_mismatched
mnli
snli
abductive_nli
fever_nli
xnli
adversarial_nli_r2
rte - Recognizing Textual Entailment
cb - Commitment Bank, similar to rte
scitail

#### Grammatic Acceptability
cola - Corpus of Linguistic Acceptability
acceptability_definiteness
acceptability_coord
acceptability_whwords
acceptability_eos


#### Unclassified
adversarial_nli_r1
adversarial_nli_r3 - SNLI+MNLI+A1+A2+A3
tatoeba - Machine translation
bucc2018 - Cross language document similarity
