+++
author = "Lillian Thistlethwaite"
title = "Introduction to Python Data Structures for R Programmers"
date = "2022-05-01"
description = "Introduction to python data structures for R programmers."
tags = [
    "python",
    "r"
]
categories = [
    "python",
    "intro-to",
]
series = ["Intro-To"]
thumbnail = "images/python-vs-r.png"
+++

## Introduction


You want to inspect a python class. First, I would look at the class structure. What are the class's members?

print(class_obj.__dict__)


import inspect
getmembers(class_obj)

dir(class_obj)



## Data Types in Python

When do you use "0.434".astype("float") and when do you use float("0.434")?
