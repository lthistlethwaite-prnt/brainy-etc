+++
title = "About"
description = "Brainy, etc., a data science blog"
date = "2022-01-24"
aliases = ["about-us", "contact"]
author = "Lillian Thistlethwaite"
+++

Brainy, etc. is a data science blog with posts centered around analysis mostly
in Python. Topics vary but may include  

* Personal finance
* Epidemiology
* Deep learning models
* Natural language processing analysis
